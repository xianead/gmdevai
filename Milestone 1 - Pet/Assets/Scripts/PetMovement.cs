﻿using UnityEngine;

public class PetMovement : MonoBehaviour
{
    public Transform goal;
    public float rotSpeed = 3;
    public float delay;
    Animator animator;

    // Update is called once per frame
    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    private void LateUpdate()
    {
        Vector3 lookAtGoal = new Vector3(goal.position.x, this.transform.position.y, goal.position.z);

        Vector3 direction = lookAtGoal - transform.position;

        this.transform.rotation = Quaternion.Slerp(this.transform.rotation,
            Quaternion.LookRotation(direction),
            Time.deltaTime * rotSpeed);

        if (Vector3.Distance(lookAtGoal, transform.position) > 5)
        {
            animator.SetInteger("Condition", 1);
            transform.position = Vector3.Lerp(this.transform.position, lookAtGoal, delay);
        }
        else
        {
            animator.SetInteger("Condition", 0);
        }
    }
}