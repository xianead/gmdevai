﻿using UnityEngine;

// Third person player movement 

public class ThirdPersonPlayerMovement : MonoBehaviour
{
    public CharacterController controller;
    public Transform cam;
    public float speed;
    // Start is called before the first frame update
    public float lookSpeedTime = 0.1f;
    float smoothVelocity;
    Animator anim;

    // Start is called before the first frame update
    private void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    private void Update()
    {
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");

        Vector3 direction = new Vector3(horizontal, 0f, vertical).normalized;

        if (direction.magnitude >= 0.1f)
        {
            // Used to find the direction the player is looking
            float lookAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + cam.eulerAngles.y;
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, lookAngle, ref smoothVelocity, lookSpeedTime);
            transform.rotation = Quaternion.Euler(0f, angle, 0f);

            if (Input.GetKeyDown(KeyCode.LeftShift))
            {
                speed = 20f;
            }

            if (Input.GetKeyUp(KeyCode.LeftShift))
            {
                speed = 12f;
            }

            if (speed == 20f)
            {
                anim.SetInteger("Condition", 2);
            }
            else
            {
                anim.SetInteger("Condition", 1);
            }

            Vector3 moveDirection = Quaternion.Euler(0, lookAngle, 0f) * Vector3.forward;
            controller.Move(moveDirection.normalized * speed * Time.deltaTime);
        }
        else
        {
            anim.SetInteger("Condition", 0);
        }

    }
}