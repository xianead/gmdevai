﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstPersonPlayerMovement : MonoBehaviour
{
    public CharacterController controller;

    private Animator animator;

    private Vector3 velocity;

    public float moveSpeed;
    public float gravity = -9.81f;
    public float jumpHeight = 3f;

    public Transform groundCheck;
    public float groundDistance = 0.4f;
    public LayerMask groundMask;

    bool isGrounded;

    // Update is called once per frame
    private void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

        if (isGrounded && velocity.y < 0)
        {
            velocity.y = -2f;
        }

        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        velocity.y += gravity * Time.deltaTime;

        controller.Move(velocity * Time.deltaTime);

        Vector3 movement = transform.right * x + transform.forward * z;

        if (Input.GetButtonDown("Jump") && isGrounded)
        {
            velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);
        }

        if (movement.magnitude >= 0.1f)
        {
            controller.Move(movement * moveSpeed * Time.deltaTime);

            if (Input.GetKeyDown(KeyCode.LeftShift))
            {
                moveSpeed = 20;
            }

            if (Input.GetKeyUp(KeyCode.LeftShift))
            {
                moveSpeed = 12;
            }
        }
    }
}