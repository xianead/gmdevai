﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPath : MonoBehaviour
{
    Transform goal;

    [SerializeField] private float speed = 5.0f;
    [SerializeField] private float accuracy = 1.0f;
    [SerializeField] private float rotSpeed = 2.0f;

    public GameObject wpManager;
    GameObject[] wps;
    GameObject currentNode;

    int currentWaypointIndex;
    int closestWpIndex;
    bool isFirstDestination;

    Graph graph;

    // Start is called before the first frame update
    void Start()
    {
        wps = wpManager.GetComponent<WaypointManager>().waypoints;
        graph = wpManager.GetComponent<WaypointManager>().graph;

        float distanceToClosestWp = Mathf.Infinity;
        GameObject closestWp = null;
        GameObject[] allWaypoints = GameObject.FindGameObjectsWithTag("wp");

        foreach (GameObject currentWaypoint in allWaypoints)
        {
            float distanceToWp = (currentWaypoint.transform.position - this.transform.position).sqrMagnitude;
            if (distanceToWp < distanceToClosestWp)
            {
                distanceToClosestWp = distanceToWp;
                closestWp = currentWaypoint;
                closestWpIndex = System.Array.IndexOf(allWaypoints, currentWaypoint);
            }
        }

        currentNode = wps[closestWpIndex + 1];

        isFirstDestination = true;
        UnityEngine.Debug.Log(isFirstDestination);
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (graph.getPathLength() == 0 || currentWaypointIndex == graph.getPathLength())
        {
            return;
        }

        // The node we are closest to at the moment
        currentNode = graph.getPathPoint(currentWaypointIndex);

        // If we are close enough to the curent waypoint, move to the next one
        if (Vector3.Distance(graph.getPathPoint(currentWaypointIndex).transform.position, transform.position) < accuracy)
        {
            currentWaypointIndex++;
        }

        // If we are not at the end of the path
        if (currentWaypointIndex < graph.getPathLength())
        {
            goal = graph.getPathPoint(currentWaypointIndex).transform;
            Vector3 lookAtGoal = new Vector3(goal.position.x, transform.position.y, goal.position.z);
            Vector3 direction = lookAtGoal - this.transform.position;

            this.transform.rotation = Quaternion.Slerp(this.transform.rotation, Quaternion.LookRotation(direction), Time.deltaTime * rotSpeed);
            this.transform.Translate(0, 0, speed * Time.deltaTime);
        }
    }

    // For testing
    public void GoToClosestIndex()
    {
        graph.AStar(currentNode, wps[closestWpIndex]);
        currentWaypointIndex = 0;
    }

    public void GoToHelipad()
    {
        if (isFirstDestination == true)
        {
            currentNode = wps[closestWpIndex];
            isFirstDestination = false;
        }

        graph.AStar(currentNode, wps[0]);
        currentWaypointIndex = 0;
    }

    public void GoToRuins()
    {
        graph.AStar(currentNode, wps[45]);
        currentWaypointIndex = 0;
    }

    public void GoToFactory()
    {
        graph.AStar(currentNode, wps[55]);
        currentWaypointIndex = 0;
    }

    public void GoToTwinMountains()
    {
        graph.AStar(currentNode, wps[14]);
        currentWaypointIndex = 0;
    }

    public void GoToMiddle()
    {
        graph.AStar(currentNode, wps[38]);
        currentWaypointIndex = 0;
    }

    public void GoToRadar()
    {
        graph.AStar(currentNode, wps[23]);
        currentWaypointIndex = 0;
    }

    public void GoToBarracks()
    {
        graph.AStar(currentNode, wps[33]);
        currentWaypointIndex = 0;
    }

    public void GoToTankers()
    {
        graph.AStar(currentNode, wps[50]);
        currentWaypointIndex = 0;
    }

    public void GoToCommandCenter()
    {
        graph.AStar(currentNode, wps[19]);
        currentWaypointIndex = 0;
    }

    public void GoToCommandPost()
    {
        graph.AStar(currentNode, wps[44]);
        currentWaypointIndex = 0;
    }

    public void GoToOilRefineryPumps()
    {
        graph.AStar(currentNode, wps[49]);
        currentWaypointIndex = 0;
    }

    private void SetWaypoint()
    {
        if (isFirstDestination)
        {
            currentNode = wps[closestWpIndex + 1];
        }
    }

}