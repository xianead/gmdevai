﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flee : NPCBaseFSM {

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {

        base.OnStateEnter(animator, stateInfo, layerIndex);

        Debug.Log("Now Fleeing");
    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {

        base.OnStateUpdate(animator, stateInfo, layerIndex);

        var fleeDirection = opponent.transform.position - NPC.transform.position;

        NPC.transform.rotation = Quaternion.Slerp(NPC.transform.rotation, Quaternion.LookRotation(-fleeDirection), base.rotSpeed * Time.deltaTime);

        NPC.transform.Translate(0, 0, (base.speed * 5f) * Time.deltaTime);
    }
}