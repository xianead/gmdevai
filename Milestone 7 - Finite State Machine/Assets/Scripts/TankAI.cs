﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankAI : MonoBehaviour {

    Animator animator;

    private int currentHP;
    private int maxHP = 100;

    public GameObject player;
    public GameObject bullet;
    public GameObject turret;

    bool isPlayerDead;

    // Start is called before the first frame update
    void Start() {

        animator = this.GetComponent<Animator>();
        currentHP = maxHP;
        isPlayerDead = false;
    }

    // Update is called once per frame
    void Update() {

        if (player != null) {

            animator.SetFloat("Distance", Vector3.Distance(transform.position, player.transform.position));
        }

        if (currentHP <= (maxHP * 0.2f)) {

            animator.SetBool("IsLowHP", true);
        }

        if (isPlayerDead == true) {

            animator.SetBool("IsPlayerDead", true);
        }
    }

    public GameObject GetPlayer() {

        return player;
    }

    void Fire() {

        GameObject bt = Instantiate(bullet, turret.transform.position, turret.transform.rotation);
        bt.GetComponent<Rigidbody>().AddForce(turret.transform.forward * 500);
    }

    public void StartFiring() {

        InvokeRepeating("Fire", 0.5f, 0.5f);
    }

    public void StopFiring() {

        CancelInvoke("Fire");
    }

    public void TakeDamage(int dmg) {

        this.currentHP -= dmg;
        CheckIfDead();
    }

    public void CheckIfDead() {

        if (this.currentHP <= 0) {

            Destroy(this.gameObject);
        }
    }

    public void SetPlayerDeath() {

        isPlayerDead = true;
        Destroy(player.gameObject);
    }
}