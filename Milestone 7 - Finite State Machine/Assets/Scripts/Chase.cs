﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chase : NPCBaseFSM {

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {

        base.OnStateEnter(animator, stateInfo, layerIndex);

        Debug.Log("Now Chasing");
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {

        base.OnStateUpdate(animator, stateInfo, layerIndex);

        if (base.opponent != null) {

            var direction = opponent.transform.position - NPC.transform.position;

            NPC.transform.rotation = Quaternion.Slerp(NPC.transform.rotation, Quaternion.LookRotation(direction), base.rotSpeed * Time.deltaTime);

            NPC.transform.Translate(0, 0, (base.speed * 3f) * Time.deltaTime);
        }
    }
}