﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

	public GameObject explosion;

	int damage = 10;

	void OnCollisionEnter(Collision col) {
		GameObject e = Instantiate(explosion, this.transform.position, Quaternion.identity);
		Destroy(e, 1.5f);
		Destroy(this.gameObject);

		if (col.collider.gameObject.CompareTag("Player")) {

			col.gameObject.GetComponent<PlayerAttack>().TakeDamage(damage);
		} else if (col.collider.gameObject.CompareTag("Enemy")) {

			col.gameObject.GetComponent<TankAI>().TakeDamage(damage);
		}
	}
}