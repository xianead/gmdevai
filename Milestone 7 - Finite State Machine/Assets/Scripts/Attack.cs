﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : NPCBaseFSM {

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {

        base.OnStateEnter(animator, stateInfo, layerIndex);
        NPC.GetComponent<TankAI>().StartFiring();

        Debug.Log("Now Attacking");
    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {

        base.OnStateUpdate(animator, stateInfo, layerIndex);

        if (base.opponent != null) {

            NPC.transform.LookAt(base.opponent.transform.position);
        }
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {

        base.OnStateExit(animator, stateInfo, layerIndex);
        NPC.GetComponent<TankAI>().StopFiring();
    }
}