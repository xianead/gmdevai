﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour {

    private int hp = 100;

    public GameObject enemy;
    public GameObject bullet;
    public GameObject gunPoint;

    private float fireRate = 2f;
    private float fireTimer;

    public HealthBar healthBar;

    void Start() {

        fireTimer = 0;

        healthBar.SetMaxHealth(hp);
    }

    // Update is called once per frame
    void Update() {

        if (Input.GetMouseButton(0) || Input.GetKeyDown(KeyCode.Space)) {

            fireTimer += 3f * Time.deltaTime;

            if (fireTimer >= fireRate) {
                Attack();
                fireTimer = 0;
            }
        }
    }

    void Attack() {

        GameObject bt = Instantiate(bullet, gunPoint.transform.position, gunPoint.transform.rotation);
        bt.GetComponent<Rigidbody>().AddForce(gunPoint.transform.forward * 500);
    }

    public void TakeDamage(int dmg) {

        this.hp -= dmg;
        healthBar.SetHealth(hp);
        CheckIfDead();
    }

    public int GetHP() {

        return this.hp;
    }

    public void CheckIfDead() {

        if (this.hp <= 0) {

            Destroy(this.gameObject);
        }
    }

    void OnDestroy() {

        enemy.GetComponent<TankAI>().SetPlayerDeath();
    }
}