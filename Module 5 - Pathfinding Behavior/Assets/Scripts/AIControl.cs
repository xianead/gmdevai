﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIControl : MonoBehaviour
{
    NavMeshAgent agent;

    public GameObject target;

    public WASDMovement playerMovement;

    Vector3 wanderTarget;

    bool isInRange;

    [SerializeField] int behaviorType = 1;

    // Start is called before the first frame update
    void Start()
    {
        isInRange = false;

        agent = this.GetComponent<NavMeshAgent>();
        playerMovement = target.GetComponent<WASDMovement>();
    }

    // Update is called once per frame
    void Update()
    {
        if (canSeeTarget() && isInRange)
        {
            switch (behaviorType)
            {
                case 1:
                    Pursue(target.transform.position);
                    break;
                case 2:
                    CleverHide();
                    break;
                case 3:
                    Evade();
                    break;
                default:
                    Debug.Log("No behavior set");
                    break;
            }
        }
        else
        {
            switch (behaviorType)
            {
                case 1:
                    Wander();
                    break;
                case 2:
                    Wander();
                    break;
                case 3:
                    Wander();
                    break;
                default:
                    Debug.Log("No behavior set");
                    break;
            }
        }

    }

    void Seek(Vector3 location)
    {
        agent.SetDestination(location);
    }

    void Flee(Vector3 location)
    {
        Vector3 fleeDirection = location - this.transform.position;
        agent.SetDestination(this.transform.position - fleeDirection);
    }

    void Pursue(Vector3 location)
    {
        Vector3 targetDirection = target.transform.position - this.transform.position;
        float lookAhead = targetDirection.magnitude / (agent.speed + playerMovement.currentSpeed);

        Seek(target.transform.position + target.transform.forward * lookAhead);
    }

    void Evade()
    {
        Vector3 targetDirection = (target.transform.position - this.transform.position);
        float lookAhead = targetDirection.magnitude / (agent.speed + playerMovement.currentSpeed);

        Flee(target.transform.position + target.transform.forward * lookAhead);
    }

    void Wander()
    {
        Debug.Log("Matthaios be Wandering");
        float wanderRadius = 20;
        float wanderDistance = 10;
        float wanderJitter = 1;

        wanderTarget += new Vector3(Random.Range(-1.0f, 1.0f) * wanderJitter, 0, Random.Range(-1.0f, 1.0f) * wanderJitter);
        wanderTarget.Normalize();
        wanderTarget *= wanderRadius;

        Vector3 targetLocal = wanderTarget + new Vector3(0, 0, wanderDistance);
        Vector3 targetWorld = this.gameObject.transform.InverseTransformDirection(targetLocal);

        Seek(targetWorld);
    }

    void Hide()
    {
        float distance = Mathf.Infinity;
        Vector3 chosenSpot = Vector3.zero;

        int hidingSpotsCount = World.Instance.GetHidingSpots().Length;

        for (int i = 0; i < hidingSpotsCount; i++)
        {
            Vector3 hideDirection = World.Instance.GetHidingSpots()[i].transform.position - target.transform.position;
            Vector3 hidePosition = World.Instance.GetHidingSpots()[i].transform.position + hideDirection.normalized * 5; // distance offset

            float spotDistance = Vector3.Distance(this.transform.position, hidePosition);

            if (spotDistance < distance)
            {
                chosenSpot = hidePosition;
                distance = spotDistance;
            }
        }

        Seek(chosenSpot);
    }

    void CleverHide()
    {
        if (Vector3.Distance(this.transform.position, target.transform.position) < 5f)
        {
            float distance = Mathf.Infinity;
            Vector3 chosenSpot = Vector3.zero;
            Vector3 chosenDirection = Vector3.zero;
            GameObject chosenGameObject = World.Instance.GetHidingSpots()[0];

            int hidingSpotsCount = World.Instance.GetHidingSpots().Length;

            for (int i = 0; i < hidingSpotsCount; i++)
            {
                Vector3 hideDirection = World.Instance.GetHidingSpots()[i].transform.position - target.transform.position;
                Vector3 hidePosition = World.Instance.GetHidingSpots()[i].transform.position + hideDirection.normalized * 5; // distance offset

                float spotDistance = Vector3.Distance(this.transform.position, hidePosition);

                if (spotDistance < distance)
                {
                    chosenSpot = hidePosition;
                    chosenDirection = hideDirection;
                    chosenGameObject = World.Instance.GetHidingSpots()[i];
                    distance = spotDistance;
                }
            }

            Collider hideCol = chosenGameObject.GetComponent<Collider>();
            Ray back = new Ray(chosenSpot, -chosenDirection.normalized);
            RaycastHit info;
            float rayDistance = 100.0f;
            hideCol.Raycast(back, out info, rayDistance);

            Seek(info.point + chosenDirection.normalized * 5);
        }
        else
        {
            return;
        }

    }

    bool canSeeTarget()
    {
        RaycastHit raycastHitInfo;
        Vector3 rayToTarget = target.transform.position - this.transform.position;

        Debug.DrawLine(this.transform.position, rayToTarget, Color.red);

        if (Physics.Raycast(this.transform.position, rayToTarget, out raycastHitInfo))
        {
            return raycastHitInfo.transform.gameObject.tag == "Player";
        }
        else
        {
            return false;
        }
    }

    public void NowInRange()
    {
        isInRange = true;
    }

    public void NotInRange()
    {
        isInRange = false;
    }
}