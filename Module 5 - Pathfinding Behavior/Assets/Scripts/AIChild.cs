﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIChild : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Contains("Player"))
        {
            Debug.Log("Hit player");
            GetComponentInParent<AIControl>().NowInRange();
        }
    }

    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag.Contains("Player"))
        {
            Debug.Log("Hit player");
            GetComponentInParent<AIControl>().NowInRange();
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag.Contains("Player"))
        {
            Debug.Log("Not in range anymore");
            GetComponentInParent<AIControl>().NotInRange();
        }
    }
}