﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIControl : MonoBehaviour {
    GameObject[] goalLocations;
    public Transform goal;

    NavMeshAgent agent;
    Animator animator;

    float speedMultiplier;
    float detectionRadius = 20;
    float fleeRadius = 10;

    // Start is called before the first frame update
    void Start() {
        goalLocations = GameObject.FindGameObjectsWithTag("Goal");

        agent = this.GetComponent<NavMeshAgent>();
        animator = this.GetComponent<Animator>();

        animator.SetFloat("wOffset", Random.Range(0.1f, 1f));

        goToDestination(goalLocations[Random.Range(0, goalLocations.Length)].transform.position);

        ResetAgent();
    }

    void LateUpdate() {
        if (agent.remainingDistance < 1) {
            goToDestination(goalLocations[Random.Range(0, goalLocations.Length)].transform.position);
        }
    }

    void ResetAgent() {
        speedMultiplier = Random.Range(0.3f, 2.0f);
        agent.speed = speedMultiplier * 2;
        agent.angularSpeed = 120;
        animator.SetFloat("speedMultiplier", speedMultiplier);

        if (speedMultiplier > 1.0f)
            animator.SetTrigger("isRunning");
        else
            animator.SetTrigger("isWalking");
    }

    public void goToDestination(Vector3 location) {
        ResetAgent();
        agent.SetDestination(location);
    }

    public void DetectNewObstacle(Vector3 location) {
        if (Vector3.Distance(location, this.transform.position) < detectionRadius) {
            Vector3 fleeDirection = (this.transform.position - location).normalized;
            Vector3 newGoal = this.transform.position + fleeDirection * fleeRadius;

            NavMeshPath path = new NavMeshPath();
            agent.CalculatePath(newGoal, path);

            if (path.status != NavMeshPathStatus.PathInvalid) {
                goToDestination(path.corners[path.corners.Length - 1]);
                animator.SetTrigger("isRunning");
                agent.speed = 10;
                agent.angularSpeed = 500;
            }
        }
    }

    public void DetectNewGoal(Vector3 location) {
        Vector3 newGoal = location;

        NavMeshPath path = new NavMeshPath();
        agent.CalculatePath(newGoal, path);

        if (path.status != NavMeshPathStatus.PathInvalid) {
            goToDestination(path.corners[path.corners.Length - 1]);
            animator.SetTrigger("isRunning");
            agent.speed = 10;
            agent.angularSpeed = 500;
        }
    }
}