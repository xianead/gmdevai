﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour {
    public GameObject obstacle;
    public GameObject treasure;

    GameObject[] agents;

    // Start is called before the first frame update
    void Start() {
        agents = GameObject.FindGameObjectsWithTag("Agent");
    }

    // Update is called once per frame
    void Update() {
        if (Input.GetMouseButtonDown(0)) {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray.origin, ray.direction, out hit)) {
                Instantiate(obstacle, hit.point, obstacle.transform.rotation);

                foreach (GameObject a in agents) {
                    a.GetComponent<AIControl>().DetectNewObstacle(hit.point);
                }
            }
        }

        if (Input.GetMouseButtonDown(1)) {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray.origin, ray.direction, out hit)) {
                Instantiate(treasure, hit.point, treasure.transform.rotation);

                foreach (GameObject x in agents) {
                    x.GetComponent<AIControl>().DetectNewGoal(hit.point);
                }
            }
        }
    }
}